package Karta;

public enum TypKarty {
	DWOJKA,
	TROJKA,
	CZWORKA,
	PIATKA,
	SZOSTKA,
	SIODEMKA,
	OSEMKA,
	DZIEWIATKA,
	DZIESIATKA,
	JEDENASTKA,
}
